## Installation

### Application setup

TODO: python setup steps

### Bot setup 

https://discordpy.readthedocs.io/en/stable/discord.html

* Create a new application from https://discord.com/developers/applications

* From the left menu, click "Bot" and "Add Bot". Confirm the action.

* Configure the bot as appropriate. Ensure:

  * The "Public Bot" option is disabled
  * The "Presence Intent" and "Server Members Intent" are enabled.

* Note the "token" for the bot.

* Go to the OAuth2 page

* Check the "bot" scope

* Enable the following Bot permissions (or Administrator for testing):

  * Manage Roles
  * Manage Channels
  * View Channels
  * Send Messages
  * Manage Messages
  * Embed Links
  * Attach Files
  * Read Message History
  * Add reactions

* Copy the generated auth link (the permissions param should equal 268561488, or 8 if just Administrator)

* Create a fresh discord server, and invite the bot (existing servers can naturally be used, but please understand how the bot manages channels and roles before doing so)

* Visit the copied link in a browser, and invite the bot to the server.

* Enable the discord integration in hunter2, create and note an API token

## Running

```
export H2_API_TOKEN=xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx # From the hunter2 instance
export H2_API_URL=http://dev.hunter2.local:8080 # Replace with your hunter2 instance base URL
export H2_DISCORD_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxx.X-xxxx.xxxxxxxxxxxxxxxxxxxxxxxxxxx" # Ensure this is the bot token and not an oauth token

python3 ./bot.py
```

## Usage

### Admin commands

`!serversetup` will create all the specified roles, categories and channels (see the top of h2bot.py). After running this for the first time, ensure any pre-existing channels are moved to the correct categories.

`!teamsearch` / `!ts` will provide a link to a team's text chat

### Global commands

`!stuck describe problem` will send a message to the admin-stuck channel with the problem text

`!joinTeam xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx` will assign the role for the relevant team (from the team page)

`!meme` / `!memes` will submit a meme for approval in the admin-meme channel. Admins can react with a pre-populated emoji to approve it or add it to a finishers meme channel.


## Hunt-specific commands

It is expected that some contributers may want to incorporate discord commands into a puzzle, without making such code publically available. This would otherwise go against the GNU AGPLv3 License
which requires modifications to be released under the same license and source code to be made available.

The `plugins` directory is .gitignore'd, and any `.py` files within it will be loaded into the bot - this allows contributers to manually upload bot plugins or use a git submodule in the plugins directory.
There is a `plugin.py.example` demonstrating how to lay out a plugin file - note that plugins make use of the "Cogs" functionality in discord.py


Copyright
=========
Hunter 2 Bot is a tool for managing a discord server for online puzzle hunts. Further information can be found at https://www.hunter2.app/ including details of contributors.

Copyright (C) 2021  The Hunter 2 contributors.

Hunter 2 Bot is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Hunter 2 Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Aferro General Public License for more details.

You should have received a copy of the GNU Aferro General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

